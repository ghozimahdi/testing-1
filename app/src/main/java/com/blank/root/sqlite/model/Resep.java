package com.blank.root.sqlite.model;

/**
 * Created by root on 17/06/16.
 */
public class Resep {
    private int id;
    private String namamakanan;
    private String deskripsi;


    public Resep(){

    }
    public Resep(int id,String namamakanan,String deskripsi){
        this.id = id;
        this.namamakanan = namamakanan;
        this.deskripsi = deskripsi;
    }
    public Resep(String namamakanan,String deskripsi){

    }
    public  int getId(){
        return(id);
    }
    public void setId(int id){
        this.id = id;
    }
    public String getNamamakanan(){
        return(namamakanan);
    }
    public void setNamamakanan(String namamakanan){
        this.namamakanan = namamakanan;
    }
    public String getDeskripsi(){
        return(deskripsi);
    }
    public void setDeskripsi(String deskripsi){
        this.deskripsi = deskripsi;
    }
}
