package com.blank.root.sqlite.Fragment;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.view.ViewGroup;
import android.widget.Toast;

import com.blank.root.sqlite.R;
import com.blank.root.sqlite.db.DatabaseHandler;
import com.blank.root.sqlite.model.Resep;

/**
 * Created by root on 18/06/16.
 */
public class FormDialog extends DialogFragment implements View.OnClickListener {
    EditText id,nama,deskripsi;
    Button simpan;
    ImageView close;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.formadd, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        //mencari id EditText
        id = (EditText)view.findViewById(R.id.id);
        nama = (EditText) view.findViewById(R.id.nama);
        deskripsi = (EditText) view.findViewById(R.id.deskripsi);
        //menacri id Button
        simpan = (Button)view.findViewById(R.id.saved);
        simpan.setOnClickListener(this);
        //mencari id ImageView clse
        close = (ImageView)view.findViewById(R.id.close);
        close.setOnClickListener(this);

        return (view);
    }
    @Override
    public void onClick(View v) {
        if(v==simpan){
            if(id.getText().toString().isEmpty()){
                id.setError("Insert Id");
            }else if (nama.getText().toString().isEmpty()){
                nama.setError("Insert Id");
            }else if(deskripsi.getText().toString().isEmpty()){
                deskripsi.setError("Insert Id");
            } else{
                DatabaseHandler handler = new DatabaseHandler((getActivity()));

                //insert data
                int dataid = Integer.parseInt(id.getText().toString());
                String datanama = nama.getText().toString();
                String datadeskripsi =deskripsi.getText().toString();

                handler.addResep(new Resep(dataid,datanama,datadeskripsi));
                Toast.makeText(getActivity(),"Harusnya Sudah Berhasil",Toast.LENGTH_LONG).show();
                dismiss();
            }
        }
        if(v ==close){
            dismiss();
        }
    }
}
