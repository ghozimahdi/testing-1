package com.blank.root.sqlite;

import android.app.Activity;
import android.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;

import com.blank.root.sqlite.Fragment.FormDialog;
import com.blank.root.sqlite.db.DatabaseHandler;
import com.blank.root.sqlite.model.Resep;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView listview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tampilData();
    }
    public void tampilData(){
        DatabaseHandler handler = new DatabaseHandler(this);
        Log.e("LOG","TAMPIL DATA");
        List<Resep> reseps = handler.getData();

        for(Resep cn : reseps){
            Log.e("ID",String.valueOf(cn.getId()));
            Log.e("NAMA",cn.getNamamakanan());
            Log.e("Deskripsi",cn.getDeskripsi());
        }
    }
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        return (true);
    }
    public boolean onOptionsItemSelected(MenuItem item){
        switch(item.getItemId()){
            case R.id.add:
                //memanggil from
                android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
                FormDialog formDialog = new FormDialog();
                formDialog.show(fm,"Form Input");

                return (true);
            default:
                return (super.onOptionsItemSelected(item));
        }
    }
}
