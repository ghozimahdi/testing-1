package com.blank.root.sqlite.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.blank.root.sqlite.model.Resep;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 17/06/16.
 */
public class DatabaseHandler extends SQLiteOpenHelper {
    //membuat Database
    private static final String DATABASE_NAME = "bukuresep";
    private static final int VERSION = 1;
    //membuat Tabel
    private static final String TABLE_NAME = "resep";

    //membuat kolom tabel
    private static final String KEY_ID = "id";
    private static final String KEY_NAMAMAKANAN = "namamakanan";
    private static final String KEY_DESKRIPSI = "deskripsi";

    public DatabaseHandler(Context context){
        //
        super(context ,DATABASE_NAME,null,VERSION);
        //
        Log.e("DATABASE","SUKSES");
    }
    public void onCreate(SQLiteDatabase db){
        //
        String SQL_TABLE = " create table " + TABLE_NAME + " ( " + KEY_ID +
                " integer primary key autoincrement not null , " + KEY_NAMAMAKANAN +
                " text , " + KEY_DESKRIPSI + " text " + " ) ";
        db.execSQL(SQL_TABLE);

    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        //
        db.execSQL(" drop table if exits " + TABLE_NAME);
        //
        onCreate(db);
    }
    public void addResep(Resep resep){
        //
        SQLiteDatabase db = this.getWritableDatabase();
        //
        ContentValues values = new ContentValues();
        //
        values.put(KEY_ID, resep.getId());
        values.put(KEY_NAMAMAKANAN, resep.getNamamakanan());
        values.put(KEY_DESKRIPSI, resep.getDeskripsi());
        //memasukan database
        db.insert(TABLE_NAME, null , values);
        //close database
        db.close();
    }
    public List<Resep> getData(){
        //
        List<Resep> dataresep = new ArrayList<Resep>();
        //select sumua data
        String SQLQUERY = " select * from " + TABLE_NAME;
        //setting pertama
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(SQLQUERY, null);
        //jika cursor pindah ke paling atas maka
        if(cursor.moveToFirst()){
            do{
                Resep resep = new Resep();
                resep.setId(Integer.parseInt(cursor.getString(0)));
                resep.setNamamakanan(cursor.getString(1));
                resep.setDeskripsi(cursor.getString(2));

                dataresep.add(resep);
            }while(cursor.moveToNext());
        }
        return(dataresep);
    }
}
